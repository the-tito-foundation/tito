#!/bin/python


from os import environ

mapping = {
    "SQLITE": 0,
    "MYSQL": 1,
    "MARIADB": 2,
    "POSTGRESQL": 3,
    "MSSQL": 4,
}


base_config =  {
   "OwnerConfig": {
     "OwnerDiscordID": environ["TITO_OWNER_ID"],
   },
   "DatabaseConfig": {
     "DatabaseType": mapping[environ["TITO_DATABASE_TYPE"]],
     "DatabaseConnectionString": "",
   },
   "BotConfig": {
     "BotSecretKey": environ["TITO_BOT_SECRET"],
     "Mute": False,
     "Deafen": False
   },
   "GuildConfig": {
     "GuildID": environ["TITO_GUILD_ID"],
     "GuildName": environ["TITO_GUILD_NAME"],
     "TextChannel": environ["TITO_GUILD_TEXT_NAME"],
     "BotVoiceChannel": environ["TITO_GUILD_VOICE_NAME"],
   }
}

if __name__ == "__main__":
    from json import dump
    try:
        base_config["DatabaseType"] = environ["TITO_DATABASE_CNX"]
    except KeyError:
        base_config["DatabaseType"] = ""
    with open("TitoConfig.json", "w") as f:
        dump(base_config, f)