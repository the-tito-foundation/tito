package parser

import (
	"strings"
	"testing"
)

func TestCli(t *testing.T) {
	t.Run("Command Line Parsing", func(t *testing.T) {
		t.Run("abc", subTestCli1)
		t.Run("'mixing up quotes\" can be tricky'", subTestCli2)
		t.Run("\"mixing up quotes' can be tricky\"", subTestCli3)
		t.Run("'mixing up quotes is fun' \"no it really is not\"", subTestCli4)
		t.Run("abc \"this is a dog\"", subTestCli5)
		t.Run("abc 123 this is a dog", subTestCli6)
		t.Run("hanging quote'", subTestCli7)
		t.Run("\"hanging quote", subTestCli8)
		t.Run("'hanging quote", subTestCli9)
		t.Run("", subTestCli10)
		t.Run("trailing spaces        more      ", subTestCli11)
	})
}

func subTestCli1(t *testing.T) {
	str := "abc"
	result := []string{"abc"}
	argv, err := ParseCli(str)

	if err != nil {
		t.Fail()
	}

	if len(argv) != len(result) {
		t.Fail()
	}

	for i := range argv {
		if strings.Compare(argv[i], result[i]) != 0 {
			t.Fail()
		}
	}
}

func subTestCli2(t *testing.T) {
	str := "'mixing up quotes\" can be tricky'"
	result := []string{"mixing up quotes\" can be tricky"}
	argv, err := ParseCli(str)

	if err != nil {
		t.Fail()
	}

	if len(argv) != len(result) {
		t.Fail()
	}

	for i := range argv {
		t.Logf("(expected : %s) vs %s", argv[i], result[i])
		if strings.Compare(argv[i], result[i]) != 0 {
			t.Fail()
		}
	}
}

func subTestCli3(t *testing.T) {
	str := "\"mixing up quotes' can be tricky\""
	result := []string{"mixing up quotes' can be tricky"}
	argv, err := ParseCli(str)

	if err != nil {
		t.Fail()
	}

	if len(argv) != len(result) {
		t.Fail()
	}

	for i := range argv {
		t.Logf("(expected : %s) vs %s", argv[i], result[i])
		if strings.Compare(argv[i], result[i]) != 0 {
			t.Fail()
		}
	}
}
func subTestCli4(t *testing.T) {
	str := "'mixing up quotes is fun' \"no it really is not\""
	result := []string{"mixing up quotes is fun", "no it really is not"}
	argv, err := ParseCli(str)

	if err != nil {
		t.Fail()
	}

	if len(argv) != len(result) {
		t.Fail()
	}

	for i := range argv {
		t.Logf("(expected : %s) vs %s", argv[i], result[i])
		if strings.Compare(argv[i], result[i]) != 0 {
			t.Fail()
		}
	}
}

func subTestCli5(t *testing.T) {
	str := "abc \"this is a dog\""
	result := []string{"abc", "this is a dog"}
	argv, err := ParseCli(str)

	if err != nil {
		t.Fail()
	}

	if len(argv) != len(result) {
		t.Fail()
	}

	for i := range argv {
		if strings.Compare(argv[i], result[i]) != 0 {
			t.Fail()
		}
	}
}

func subTestCli6(t *testing.T) {
	str := "abc 123 this is a dog"
	result := []string{"abc", "123", "this", "is", "a", "dog"}
	argv, err := ParseCli(str)

	if err != nil {
		t.Fail()
	}

	if len(argv) != len(result) {
		t.Fail()
	}

	for i := range argv {
		if strings.Compare(argv[i], result[i]) != 0 {
			t.Fail()
		}
	}
}

func subTestCli7(t *testing.T) {
	str := "hanging quote'"
	result := []string{"hanging", "quote'"}
	argv, err := ParseCli(str)

	if err != nil {
		t.Fail()
	}

	if len(argv) != len(result) {
		t.Fail()
	}

	for i := range argv {
		t.Logf("(expected : %s) vs %s", argv[i], result[i])
		if strings.Compare(argv[i], result[i]) != 0 {
			t.Fail()
		}
	}
}

func subTestCli8(t *testing.T) {
	str := "'hanging quote"
	_, err := ParseCli(str)

	if err == nil {
		t.Fail()
	}
}

func subTestCli9(t *testing.T) {
	str := "\"hanging quote"
	_, err := ParseCli(str)

	if err == nil {
		t.Fail()
	}
}

func subTestCli10(t *testing.T) {
	str := ""
	_, err := ParseCli(str)

	if err != nil {
		t.Fail()
	}
}

func subTestCli11(t *testing.T) {
	str := "trailing spaces        more      "
	result := []string{"trailing", "spaces", "more"}
	argv, err := ParseCli(str)

	if err != nil {
		t.Fail()
	}

	for i := range argv {
		if strings.Compare(argv[i], result[i]) != 0 {
			t.Fail()
		}
	}

}
