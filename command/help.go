package command

import (
	"fmt"

	"gitlab.com/the-tito-foundation/tito/database"
)

// Help - display tito's commands
func Help(discordID string) ([]Response, error) {

	str := ""

	user, err := database.SelectUser(database.User{DiscordID: discordID})
	if err != nil {
		return []Response{}, err
	}

	for reqPermission, commands := range helpList {
		if reqPermission > user.Role {
			continue
		}

		for command, text := range commands {
			str += fmt.Sprintf("!%s\n\t%s", command, text)
		}
	}

	response := Response{
		ShouldRespondToChannel: false,
		ShouldRespondToUser:    true,
		ShouldSendVoice:        false,
		VoiceOutput:            nil,
		Message:                str,
	}

	return []Response{response}, nil
}
