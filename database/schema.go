package database

import (
	"time"
)

// UserRole enum - represents permissions levels
type UserRole uint8

// Definitions of UserRole Values - OWNER being the highest, INACTIVE being the lowest
const (
	INACTIVE              = 0
	UNREGISTERED          = 1
	REGISTERED            = 2
	MEMBER                = 3
	OFFICER               = 4 // OP
	ADMIN                 = 5
	OWNER        UserRole = 6
)

// User - represents a user in the database
type User struct {
	ID        uint64    `gorm:"PRIMARY_KEY"`
	DiscordID string    `gorm:"UNIQUE;NOT NULL"`
	Username  string    `gorm:"NOT NULL"`
	Nickname  string    `gorm:"NOT NULL"`
	RealName  string    `gorm:"NOT NULL"`
	Role      UserRole  `gorm:"DEFAULT:0"`
	Games     []Game    `gorm:"many2many:GameUser;"`
	Messages  []Message `gorm:"many2many:MessageUser;"`
}

// Message - represents a message in the database
type Message struct {
	ID             uint64 `gorm:"PRIMARY_KEY"`
	DateFirstSent  time.Time
	MessageContent string `gorm:"UNIQUE;NOT NULL"`
	Senders        []User `gorm:"many2many:MessageUser;"`
}

// Game - represents a game in the database
type Game struct {
	ID         uint64 `gorm:"PRIMARY_KEY"`
	Name       string `gorm:"UNIQUE;NOT NULL"`
	MaxPlayers uint8
	Owners     []User `gorm:"many2many:GameUser;"`
}
