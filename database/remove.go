package database

import (
	"log"
)

// DeleteUser - Delete a user.
func DeleteUser(user User) error {

	if b, _ := IsInUserDB(user); !b {
		return errDoesNotExist
	}

	err := dbConn.Delete(user).Error
	if err != nil {
		log.Println(err)
	}
	return err
}

// DeleteGame - Delete a game.
func DeleteGame(game Game) error {

	if b, _ := IsInGameDB(game); !b {
		return errDoesNotExist
	}

	err := dbConn.Delete(game).Error
	if err != nil {
		log.Println(err)
	}
	return err
}

// DeleteMessage - Delete a message.
func DeleteMessage(message Message) error {

	if b, _ := IsInMsgDB(message); !b {
		return errDoesNotExist
	}

	err := dbConn.Delete(message).Error
	if err != nil {
		log.Println(err)
	}
	return err
}
