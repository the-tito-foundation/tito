package discord

import (
	"log"
	"strings"
	"sync"

	"github.com/bwmarrin/discordgo"
	"gitlab.com/the-tito-foundation/tito/config"
)

var textChannelID string
var voiceChannelID string
var session *discordgo.Session
var voiceConnection *discordgo.VoiceConnection

// StartDiscord - start the discord portion of the bot
// this finds and records the specified text-channel and voice channel
// it also records the discordSession and joins voice if necessary
func StartDiscord() error {
	b, err := discordgo.New("Bot " + config.Cfg.BotConfig.BotSecretKey)
	if err != nil {
		log.Fatalln(err)
	}

	err = b.Open()
	if err != nil {
		log.Fatalln(err)
	}

	channels, err := b.GuildChannels(config.Cfg.GuildConfig.GuildID)
	if err != nil {
		log.Fatalln(err)
	}

	for _, ch := range channels {

		// If not muted and deafened

		if config.Cfg.BotConfig.Mute != config.Cfg.BotConfig.Deafen &&
			!config.Cfg.BotConfig.Mute {

			// if it's a voice channel and the name matches our configration guild name
			if ch.Type == discordgo.ChannelTypeGuildVoice &&
				strings.Compare(config.Cfg.GuildConfig.BotVoiceChannel, ch.Name) == 0 {

				voiceChannelID = ch.ID
				// join voice
				voiceConnection, err =
					b.ChannelVoiceJoin(ch.GuildID,
						ch.ID,
						config.Cfg.BotConfig.Mute,
						config.Cfg.BotConfig.Deafen)
				if err != nil {
					return err
				}

				//TODO: start the audio component with the vc connection
			}
		}

		// if it's a text channel and the name matches the config
		if ch.Type == discordgo.ChannelTypeGuildText &&
			strings.Compare(ch.Name, config.Cfg.GuildConfig.TextChannel) == 0 {

			log.Println("Found Text Channel")
			// save the ID
			textChannelID = ch.ID
		}
	}

	b.AddHandler(routeCommand)

	voiceSync = new(sync.Mutex)
	// finally attach the session
	session = b
	b = nil

	return nil
}

// StopDiscord - stop the discord portion of the bot, closing the VoiceConnection and Session
// if these were opened
func StopDiscord() {
	if voiceConnection != nil {
		voiceConnection.Close()
		voiceConnection = nil
	}
	if session != nil {
		session.Close()
		session = nil
	}

	textChannelID = ""
	voiceChannelID = ""
}
