#!/bin/bash
#
# Code coverage generation

COVERAGE_DIR="${COVERAGE_DIR:-coverage}"

# Create the coverage files directory
mkdir -p "$COVERAGE_DIR";

# Create a coverage file for each package
for package in ${PKG_LIST}; do
    go test -short -covermode=count -coverprofile "${COVERAGE_DIR}/${package##*/}.cov" "$package" ;
    if [ -f "${COVERAGE_DIR}/coverage.cov" ]
    then
        cat "${COVERAGE_DIR}/${package##*/}.cov" | tail -n +2 >> "${COVERAGE_DIR}/coverage.cov"
    else
        cat "${COVERAGE_DIR}/${package##*/}.cov" > "${COVERAGE_DIR}/coverage.cov"
    fi
    rm "${COVERAGE_DIR}/${package##*/}.cov"
done ;

# Display the global code coverage
go tool cover -func="${COVERAGE_DIR}"/coverage.cov ;

# If needed, generate HTML report
if [ "$1" == "html" ]; then
    go tool cover -html="${COVERAGE_DIR}"/coverage.cov -o coverage.html ;
fi

# Remove the coverage files directory
rm -rf "$COVERAGE_DIR";