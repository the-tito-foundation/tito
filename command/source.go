package command

import "fmt"

const respositoryURL = "https://gitlab.com/the-tito-foundation/tito"

// Source - give the URL for the repository for Tito :)
func Source() ([]Response, error) {

	response := Response{
		ShouldRespondToUser:    false,
		ShouldRespondToChannel: true,
		Message:                fmt.Sprintf("I am currently hosted @ %s", respositoryURL),
	}

	return []Response{response}, nil

}
