package command

import "gitlab.com/the-tito-foundation/tito/database"

// Reg - register the user with Tito for advanced features (really all it is it records the real name)
// so whois works.
func Reg(discordID, realname string) ([]Response, error) {
	u := database.User{DiscordID: discordID}

	inDb, err := database.SelectUser(u)
	if err != nil {
		return []Response{}, err
	}

	u.RealName = realname
	if inDb.Role < database.REGISTERED {
		u.Role = database.REGISTERED
	}

	err = database.UpdateUser(u)
	if err != nil {
		return []Response{}, err
	}

	return []Response{}, nil
}
