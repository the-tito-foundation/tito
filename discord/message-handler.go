package discord

import (
	"errors"
	"io"
	"log"
	"strconv"
	"strings"
	"sync"

	"gitlab.com/the-tito-foundation/tito/database"

	"gitlab.com/the-tito-foundation/tito/command"

	"github.com/bwmarrin/discordgo"
	"github.com/jonas747/dca"
	"gitlab.com/the-tito-foundation/tito/parser"
)

var voiceSync *sync.Mutex
var errInvalidCommand = errors.New("invalid command")
var errCommandNotFound = errors.New("command not found")

func routeCommand(s *discordgo.Session, m *discordgo.MessageCreate) {
	// Best Effort
	userObj, err := addElementsToDB(m)
	if err != nil {
		log.Println(err)
	}

	argv, err := validateCommand(m)
	if err != nil {
		return
	}

	var response []command.Response

	if permissionLevel, ok := command.PermissionList[argv[0]]; ok {
		if userObj.Role < permissionLevel {
			log.Printf("User '%s' with Role: '%d' does not meet permission level '%d' for command '%s'",
				userObj.Username,
				userObj.Role,
				permissionLevel,
				argv[0])
			return
		}
	}

	switch argv[0] {
	case command.AddGameCmd:
		val, err := strconv.ParseUint(argv[2], 10, 8)
		if err != nil {
			log.Println(err)
			return
		}
		response, err = command.AddGame(m.Author.ID, argv[1], uint8(val))
		break
	case command.DeopCmd:
		u, err := database.SelectUserByNickname(argv[1])
		if err != nil {
			return
		}
		response, err = command.Deop(u.DiscordID)
		break
	case command.DisableCmd:
		u, err := database.SelectUserByNickname(argv[1])
		if err != nil {
			return
		}
		response, err = command.Disable(u.DiscordID)
		break
	case command.EnableCmd:
		u, err := database.SelectUserByNickname(argv[1])
		if err != nil {
			return
		}
		response, err = command.Enable(u.DiscordID)
		break
	case command.GamesCmd:
		val, err := strconv.ParseUint(argv[2], 10, 8)
		if err != nil {
			log.Println(err)
			return
		}
		response, err = command.Games(uint8(val))
		break
	case command.HelpCmd:
		response, err = command.Help(m.Author.ID)
		break
	case command.InspireCmd:
		response, err = command.Inspire()
		break
	case command.LaughCmd:
		response, err = command.Laugh()
		break
	case command.OpCmd:
		u, err := database.SelectUserByNickname(argv[1])
		if err != nil {
			return
		}
		response, err = command.Op(u.DiscordID)
		break
	case command.PlayCmd:
		response, err = command.Play(argv[1])
		break
	case command.RegCmd:
		u, err := database.SelectUserByNickname(argv[1])
		if err != nil {
			return
		}
		response, err = command.Reg(u.DiscordID, argv[2])
		break
	case command.SourceCmd:
		response, err = command.Source()
		break
	case command.WhoIsCmd:
		u, err := database.SelectUserByNickname(argv[1])
		if err != nil {
			return
		}
		response, err = command.WhoIs(u.DiscordID)
		break
	case command.SummonCmd:
		response, err = command.Summon(argv[1])
		if err != nil {
			return
		}
		break
	case command.PollCmd:
		response, err = command.Poll(argv[1:])
		if err != nil {
			return
		}
	default:
		log.Printf("Command '%v' unrecognized\n", argv[0])
	}

	for _, r := range response {

		if r.ShouldRespondToChannel {
			msg, err := session.ChannelMessageSend(
				textChannelID,
				r.Message)

			if err != nil {
				log.Println(err)
				return
			}

			for _, v := range r.ReactionsToAdd {
				session.MessageReactionAdd(textChannelID, msg.ID, v)
			}
		}

		if r.VoiceOutput != nil {
			defer r.VoiceOutput.Cleanup()
		}

		if r.ShouldRespondToUser {
			channel, err := session.UserChannelCreate(m.Author.ID)
			if err != nil {
				log.Println(err)
				return
			}
			msg, err := session.ChannelMessageSend(channel.ID, r.Message)
			if err != nil {
				log.Println(err)
				return
			}

			for _, v := range r.ReactionsToAdd {
				session.MessageReactionAdd(textChannelID, msg.ID, v)
			}
		}

		if r.ShouldSendVoice {
			voiceSync.Lock()
			defer voiceSync.Unlock()
			done := make(chan error)
			dca.NewStream(r.VoiceOutput, voiceConnection, done)
			err = <-done
			if err != nil && err != io.EOF {
				log.Println(err)
				return
			}
		}
	}
}

func validateCommand(m *discordgo.MessageCreate) ([]string, error) {
	// all bot command start with !
	if !strings.HasPrefix(m.Content, "!") {
		return nil, errInvalidCommand
	}

	if len(m.Content[1:]) == 0 {
		return nil, errInvalidCommand
	}

	argv, err := parser.ParseCli(m.Content[1:])
	if err != nil {
		return nil, err
	}

	argv[0] = strings.ToLower(argv[0])

	if _, ok := command.ProgrammedCommands[argv[0]]; !ok {
		log.Println(errCommandNotFound)
		return nil, errCommandNotFound
	}

	return argv, nil
}
