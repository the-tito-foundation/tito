package config

import (
	"os"
	"strings"
	"testing"

	"gitlab.com/the-tito-foundation/tito/database"
)

var testCfg = Configuration{
	OwnerConfig: OwnerOptions{
		OwnerDiscordID: "12345678910",
	},
	GuildConfig: GuildOptions{
		GuildID:         "12345678910",
		GuildName:       "Somewhere in California?",
		TextChannel:     "The Shore-Shack",
		BotVoiceChannel: "ARealVoiceChannel",
	},
	DatabaseConfig: DatabaseOptions{
		DatabaseType:             database.SQLITE,
		DatabaseConnectionString: "testdb.sqlite3",
	},
	BotConfig: BotOptions{
		BotSecretKey: "TotallySecretKey",
		Mute:         true,
		Deafen:       true,
	},
}

func TestLoadConfig(t *testing.T) {
	WriteSampleConfig(t)
	defer database.CloseDB()

	err := LoadConfig()
	if err != nil {
		t.Error(err)
	}

	if strings.Compare(testCfg.BotConfig.BotSecretKey, Cfg.BotConfig.BotSecretKey) != 0 {
		t.Errorf("%s != %s", testCfg.BotConfig.BotSecretKey, Cfg.BotConfig.BotSecretKey)
	}
	if strings.Compare(testCfg.GuildConfig.GuildID, Cfg.GuildConfig.GuildID) != 0 {
		t.Errorf("%s != %s", testCfg.GuildConfig.GuildID, Cfg.GuildConfig.GuildID)
	}

	if strings.Compare(testCfg.GuildConfig.GuildName, Cfg.GuildConfig.GuildName) != 0 {
		t.Errorf("%s != %s", testCfg.GuildConfig.GuildName, Cfg.GuildConfig.GuildName)
	}

	if strings.Compare(testCfg.GuildConfig.TextChannel, Cfg.GuildConfig.TextChannel) != 0 {
		t.Errorf("%s != %s", testCfg.GuildConfig.TextChannel, Cfg.GuildConfig.TextChannel)
	}

	if strings.Compare(testCfg.GuildConfig.BotVoiceChannel, Cfg.GuildConfig.BotVoiceChannel) != 0 {
		t.Errorf("%s != %s", testCfg.GuildConfig.BotVoiceChannel, Cfg.GuildConfig.BotVoiceChannel)
	}

	if strings.Compare(testCfg.DatabaseConfig.DatabaseConnectionString, Cfg.DatabaseConfig.DatabaseConnectionString) != 0 {
		t.Errorf("%s != %s", testCfg.DatabaseConfig.DatabaseConnectionString, Cfg.DatabaseConfig.DatabaseConnectionString)
	}

	if strings.Compare(testCfg.OwnerConfig.OwnerDiscordID, Cfg.OwnerConfig.OwnerDiscordID) != 0 {
		t.Errorf("%s != %s", testCfg.OwnerConfig.OwnerDiscordID, Cfg.OwnerConfig.OwnerDiscordID)
	}

	if testCfg.BotConfig.Mute != Cfg.BotConfig.Mute {
		t.Errorf("%v != %v", testCfg.BotConfig.Mute, Cfg.BotConfig.Mute)
	}

	if testCfg.BotConfig.Deafen != Cfg.BotConfig.Deafen {
		t.Errorf("%v != %v", testCfg.BotConfig.Deafen, Cfg.BotConfig.Deafen)
	}

	if testCfg.DatabaseConfig.DatabaseType != Cfg.DatabaseConfig.DatabaseType {
		t.Errorf("%d != %d", testCfg.DatabaseConfig.DatabaseType, Cfg.DatabaseConfig.DatabaseType)
	}
}

func TestLoadDefaultConfig(t *testing.T) {
	DestroyConfig(t)
	defer database.CloseDB()
	err := LoadConfig()
	if err == nil {
		t.Error(err)
	}
}

func TestLoadConfigNecessaryFields(t *testing.T) {
	defer database.CloseDB()
	err := LoadConfig()
	if err == nil {
		t.Error(err)
	}
}

func DestroyConfig(t *testing.T) {
	err := os.Remove(cfgFileName)
	if err != nil && !os.IsNotExist(err) {
		t.Error(err)
	}

	err = os.Remove("testdb.sqlite3")
	if err != nil && !os.IsNotExist(err) {
		t.Error(err)
	}

	err = os.Remove(defaultDB)
	if err != nil && !os.IsNotExist(err) {
		t.Error(err)
	}
}

func WriteSampleConfig(t *testing.T) {
	DestroyConfig(t)
	err := dumpConfig(&testCfg)

	if err != nil {
		t.Error(err)
	}
}
