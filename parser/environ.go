package parser

import (
	"errors"
	"fmt"
	"log"
)

var errNoSuchKey = errors.New("no such key")

var volatileEnviron = make(map[string]string)

// SetEnviron - set the environment variable indicated
// by key to the value
func SetEnviron(key, val string) {
	if _, ok := volatileEnviron[key]; ok {
		log.Printf("[WARN]: Key Already Exists")
	}
	volatileEnviron[key] = val
}

// GetEnviron - get the environment variable indicated
// by key
func GetEnviron(key string) (string, error) {
	if v, ok := volatileEnviron[key]; ok {
		return v, nil
	}

	log.Printf("%s (%s)", errNoSuchKey, key)
	return "", errNoSuchKey
}

// ListEnv - list all environment variables currently held in the map
func ListEnv() string {
	ret := ""
	for key, v := range volatileEnviron {
		ret += fmt.Sprintf("%s = %s\n", key, v)
	}

	return ret
}
