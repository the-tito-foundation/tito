package main

import (
	"log"

	"gitlab.com/the-tito-foundation/tito/config"
	"gitlab.com/the-tito-foundation/tito/discord"
	"gitlab.com/the-tito-foundation/tito/logging"
)

func main() {
	killchan := make(chan bool, 1)
	err := config.LoadConfig()
	if err != nil {
		log.Fatalln(err)
	}

	err = logging.StartLogging("tito.log")
	if err != nil {
		log.Fatalln(err)
	}
	defer logging.StopLogging()

	err = discord.StartDiscord()
	if err != nil {
		log.Fatalln(err)
	}

	<-killchan

	defer discord.StopDiscord()
}
