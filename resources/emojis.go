package resources

const voteSubdir = "vote/"

const humanReadable = "human/"

var emojiVote = []string{
	":zero:",
	":one:",
	":two:",
	":three:",
	":four:",
	":five:",
	":six:",
	":seven:",
	":eight:",
	":nine:",
}

const computerReadable = "computer/"

var emojiUnicode = []string{
	"\u0030\u20E3",
	"\u0031\u20E3",
	"\u0032\u20E3",
	"\u0033\u20E3",
	"\u0034\u20E3",
	"\u0035\u20E3",
	"\u0036\u20E3",
	"\u0037\u20E3",
	"\u0038\u20E3",
	"\u0039\u20E3",
}

func loadEmojiEnglish(index int) (string, error) {
	if index < 0 || index > 9 {
		return "", errNoResource
	}
	return emojiVote[index], nil
}

func loadEmojiComputer(index int) (string, error) {
	if index < 0 || index > 9 {
		return "", errNoResource
	}
	return emojiUnicode[index], nil
}
