package command

import "gitlab.com/the-tito-foundation/tito/database"

// Enable - allow a player to use the Tito.
func Enable(discordID string) ([]Response, error) {
	u := database.User{DiscordID: discordID}

	inDb, err := database.SelectUser(u)
	if err != nil {
		return []Response{}, err
	}

	if inDb.Role == database.INACTIVE {
		u.Role = database.UNREGISTERED
		err := database.UpdateUser(u)
		if err != nil {
			return []Response{}, err
		}
	}

	return []Response{}, nil
}
