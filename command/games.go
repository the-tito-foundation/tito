package command

import (
	"fmt"

	"gitlab.com/the-tito-foundation/tito/database"
)

// Games - list the games that can be played by the number passed, considering
// player count
func Games(players uint8) ([]Response, error) {

	games, err := database.SelectGamesForPlayerCount(players)
	if err != nil {
		return []Response{}, err
	}

	message := ""

	for _, v := range games {
		message += fmt.Sprintf("%s (%d)\n", v.Name, v.MaxPlayers)
		for _, u := range v.Owners {
			message += fmt.Sprintf("\t%s\n", u.Nickname)
		}
	}

	response := Response{
			ShouldRespondToChannel: true,
			ShouldRespondToUser:    false,
			Message:                message,
		}

	return []Response{response}, nil

}
