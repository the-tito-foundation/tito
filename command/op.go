package command

import "gitlab.com/the-tito-foundation/tito/database"

// Op - op a player, unlocking various advanced administrative features.
func Op(discordID string) ([]Response, error) {
	u := database.User{DiscordID: discordID}

	inDb, err := database.SelectUser(u)
	if err != nil {
		return []Response{}, err
	}

	if inDb.Role < database.OFFICER {
		u.Role = database.OFFICER
		err := database.UpdateUser(u)
		if err != nil {
			return []Response{}, err
		}
	}

	return []Response{}, nil
}
