package database

import (
	"os"
	"testing"
	"time"
)

const DBURI = "testdb.sqlite"
const DBTYPE = SQLITE

var Users = []User{
	{
		DiscordID: "0",
		Username:  "Testttes",
		Nickname:  "GlADoS",
		RealName:  "Wheatley",
		Role:      ADMIN,
		Games:     nil,
		Messages:  nil,
	},
	{
		DiscordID: "123217",
		Username:  "Testes",
		Nickname:  "John",
		RealName:  "Cave Johnson",
		Role:      UNREGISTERED,
		Games:     nil,
		Messages:  nil,
	}}

var Games = []Game{
	{
		Name:       "Game 1",
		MaxPlayers: 4,
		Owners:     nil,
	},

	{
		Name:       "Game 2",
		MaxPlayers: 6,
		Owners:     nil,
	},
	{
		Name:       "Game 3",
		MaxPlayers: 2,
		Owners:     nil,
	},
	{
		Name:       "Game 4",
		MaxPlayers: 2,
		Owners:     nil,
	},
}

var Messages = []Message{
	{
		DateFirstSent:  time.Now(),
		MessageContent: "Hello,World",
		Senders:        nil,
	},
	{
		DateFirstSent:  time.Now(),
		MessageContent: "Hold the Pickle",
		Senders:        nil,
	},
	{
		DateFirstSent:  time.Now(),
		MessageContent: "ALOOOOOOOOOHA",
		Senders:        nil,
	},
}

func Setup(t *testing.T) {
	if _, err := os.Stat(DBURI); err == nil {
		os.Remove(DBURI)
	}

	err := ConnectToDB(DBTYPE, DBURI)
	if err != nil {
		t.Error()
	}
}

func Teardown(t *testing.T) {
	err := CloseDB()
	if err != nil {
		t.Error()
	}
	if _, err := os.Stat(DBURI); err == nil {
		os.Remove(DBURI)
	}
}

func InsertionTests(t *testing.T) {
	err := InsertUser(Users[0])
	if err != nil {
		t.Error(err)
	}

	err = InsertUser(Users[1])
	if err != nil {
		t.Error(err)
	}

	err = InsertGame(Games[0])
	if err != nil {
		t.Error(err)
	}

	err = InsertGame(Games[1])
	if err != nil {
		t.Error(err)
	}

	err = InsertGame(Games[2])
	if err != nil {
		t.Error(err)
	}

	err = InsertGame(Games[3])
	if err != nil {
		t.Error(err)
	}

	err = InsertMessage(Messages[0])
	if err != nil {
		t.Error(err)
	}

	err = InsertMessage(Messages[1])
	if err != nil {
		t.Error(err)
	}

	err = InsertMessage(Messages[2])
	if err != nil {
		t.Error(err)
	}

	err = InsertUserMessageAssoc(Messages[0], Users[0])
	if err != nil {
		t.Error(err)
	}

	err = InsertUserMessageAssoc(Messages[0], Users[1])
	if err != nil {
		t.Error(err)
	}

	err = InsertUserMessageAssoc(Messages[2], Users[1])
	if err != nil {
		t.Error(err)
	}

	err = InsertGameUserAssoc(Games[0], Users[0])
	if err != nil {
		t.Error(err)
	}

	err = InsertGameUserAssoc(Games[1], Users[0])
	if err != nil {
		t.Error(err)
	}

	err = InsertGameUserAssoc(Games[2], Users[0])
	if err != nil {
		t.Error(err)
	}

	err = InsertGameUserAssoc(Games[3], Users[1])
	if err != nil {
		t.Error(err)
	}

	err = InsertGameUserAssoc(Games[1], Users[1])
	if err != nil {
		t.Error(err)
	}
}

func SelectQueryTest(t *testing.T) {

	if b, err := IsInGameDB(Games[0]); !b {
		t.Error(err)
	}

	if b, err := IsInUserDB(Users[0]); !b {
		t.Error(err)
	}

	if b, err := IsInMsgDB(Messages[0]); !b {
		t.Error(err)
	}

	Games[0].Owners = nil

	_, err := SelectGame(Games[0])
	if err != nil {
		t.Error(err)
	}

	games, err := SelectGamesForPlayerCount(3)
	if err != nil || games == nil || len(games) != 2 {
		t.Error(err)
	}

	_, err = SelectUser(Users[0])
	if err != nil {
		t.Error(err)
	}
	_, err = SelectUserByUsername(Users[1].Username)
	if err != nil {
		t.Error(err)
	}

	_, err = SelectUserByNickname(Users[1].Nickname)
	if err != nil {
		t.Error(err)
	}
}

func RemovalTest(t *testing.T) {
	err := DeleteGame(Games[0])
	if err != nil {
		t.Error(err)
	}

	if b, err := IsInGameDB(Games[0]); b {
		t.Error(err)
	}

	err = DeleteUser(Users[0])
	if err != nil {
		t.Error(err)
	}

	if b, err := IsInUserDB(Users[0]); b {
		t.Error(err)
	}

	err = DeleteMessage(Messages[0])
	if err != nil {
		t.Error(err)
	}

	if b, err := IsInMsgDB(Messages[0]); b {
		t.Error(err)
	}
}

func UpdateTest(t *testing.T) {
	g, err := SelectGame(Games[0])
	if err != nil {
		t.Error(err)
	}

	err = UpdateGame(g)
	if err != nil {
		t.Error(err)
	}

	u, err := SelectUser(Users[0])
	if err != nil {
		t.Error(err)
	}

	err = UpdateUser(u)
	if err != nil {
		t.Error(err)
	}

	msg, err := SelectMessage(Messages[0])
	if err != nil {
		t.Error(err)
	}

	err = UpdateMessage(msg)
	if err != nil {
		t.Error(err)
	}
}

func TestTeardownLock(t *testing.T) {
	err := CloseDB()
	if err == nil {
		t.Error()
	}
}
func TestDBNegative(t *testing.T) {
	Setup(t)
	defer Teardown(t)

	t.Run("Select-", SelectNegativeTest)
	t.Run("Delete-", DeleteNegativeTest)
	t.Run("Update-", UpdateNegativeTest)
	t.Run("Insert-", InsertNegativeTest)
}

func TestDBPositive(t *testing.T) {
	Setup(t)
	defer Teardown(t)

	t.Run("Insert+", InsertionTests)
	t.Run("Select+", SelectQueryTest)
	t.Run("UpdateTest+", UpdateTest)
	t.Run("Delete+", RemovalTest)
}

func InsertNegativeTest(t *testing.T) {
	err := InsertUser(Users[0])
	if err != nil {
		t.Error(err)
	}

	err = InsertUser(User{DiscordID: "0"})
	if err == nil {
		t.Error(err)
	}

	err = InsertGame(Games[0])
	if err != nil {
		t.Error(err)
	}

	err = InsertGame(Game{Name: "Game 1"})
	if err == nil {
		t.Error(err)
	}

	err = InsertMessage(Messages[0])
	if err != nil {
		t.Error(err)
	}

	err = InsertMessage(Message{MessageContent: "Hello,World"})
	if err == nil {
		t.Error(err)
	}
}

func SelectNegativeTest(t *testing.T) {

	if b, err := IsInGameDB(Games[0]); b {
		t.Error(err)
	}

	if b, err := IsInUserDB(Users[0]); b {
		t.Error(err)
	}

	if b, err := IsInMsgDB(Messages[0]); b {
		t.Error(err)
	}

	_, err := SelectGame(Games[0])
	if err == nil {
		t.Error(err)
	}

	games, err := SelectGamesForPlayerCount(3)
	if len(games) != 0 && games != nil {
		t.Error(err)
	}

	_, err = SelectUser(Users[0])
	if err == nil {
		t.Error(err)
	}

	_, err = SelectUserByUsername(Users[1].Username)
	if err == nil {
		t.Error(err)
	}

	_, err = SelectUserByNickname(Users[1].Nickname)
	if err == nil {
		t.Error(err)
	}
}

func DeleteNegativeTest(t *testing.T) {
	err := DeleteGame(Games[0])
	if err == nil {
		t.Error(err)
	}

	if b, err := IsInGameDB(Games[0]); b {
		t.Error(err)
	}

	err = DeleteUser(Users[0])
	if err == nil {
		t.Error(err)
	}

	if b, err := IsInUserDB(Users[0]); b {
		t.Error(err)
	}

	err = DeleteMessage(Messages[0])
	if err == nil {
		t.Error(err)
	}

	if b, err := IsInMsgDB(Messages[0]); b {
		t.Error(err)
	}
}

func UpdateNegativeTest(t *testing.T) {

	err := UpdateGame(Game{})
	if err == nil {
		t.Error(err)
	}

	err = UpdateUser(User{})
	if err == nil {
		t.Error(err)
	}

	err = UpdateMessage(Message{})
	if err == nil {
		t.Error(err)
	}
}
