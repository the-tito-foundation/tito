package command

import "gitlab.com/the-tito-foundation/tito/database"

// Disable - remove a player's ability to use Tito.
func Disable(discordID string) ([]Response, error) {
	u := database.User{DiscordID: discordID}

	inDb, err := database.SelectUser(u)
	if err != nil {
		return []Response{}, err
	}

	if inDb.Role > database.INACTIVE {
		u.Role = database.INACTIVE
		err := database.UpdateUser(u)
		if err != nil {
			return []Response{}, err
		}
	}

	return []Response{}, nil
}
