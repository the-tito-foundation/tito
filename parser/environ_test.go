package parser

import (
	"fmt"
	"strings"
	"testing"
)

const TestKey = "!TEST"
const TestValue = "12345"
const TestValue2 = "54321"

func TestEnviron(t *testing.T) {
	volatileEnviron = make(map[string]string)

	t.Run("Store/Get Environ", func(t *testing.T) {
		SetEnviron(TestKey, TestValue)
		v, err := GetEnviron(TestKey)
		if err != nil {
			t.Error(err)
		}

		if strings.Compare(v, TestValue) != 0 {
			t.Fail()
		}
	})

	t.Run("ListEnv", func(t *testing.T) {
		str := ListEnv()

		if strings.Compare(str, fmt.Sprintf("%s = %s\n", TestKey, TestValue)) != 0 {
			t.Fail()
		}
	})

	t.Run("Store/Overwrite/GetEnviron", func(t *testing.T) {
		SetEnviron(TestKey, TestValue2)
		v, err := GetEnviron(TestKey)
		if err != nil {
			t.Error(err)
		}

		if strings.Compare(v, TestValue2) != 0 {
			t.Fail()
		}
	})

	volatileEnviron = make(map[string]string)

	t.Run("(Fail) GetEnviron", func(t *testing.T) {
		_, err := GetEnviron(TestKey)

		if err == nil {
			t.Fail()
		}
	})
}
