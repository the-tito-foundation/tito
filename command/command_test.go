package command

import (
	"errors"
	"fmt"
	"os"
	"strings"
	"testing"

	"gitlab.com/the-tito-foundation/tito/database"
)

const HappyJinjoHouse = "https://www.youtube.com/watch?v=Kxcfk2mMHxQ"
const DBTYPE = database.SQLITE
const DBURI = "testdb.sqlite3"

var errAssertFail = errors.New("assert failure")

var UsersList = []database.User{
	{
		DiscordID: "0",
		Username:  "Testttes",
		Nickname:  "GlADoS",
		RealName:  "Wheatley",
		Role:      database.OWNER,
		Games:     nil,
		Messages:  nil,
	},
	{
		DiscordID: "123217",
		Username:  "Testes",
		Nickname:  "John",
		RealName:  "",
		Role:      database.INACTIVE,
		Games:     nil,
		Messages:  nil,
	},
	{
		DiscordID: "123145123",
		Username:  "WorldHello",
		Nickname:  "Tito",
		RealName:  "Raymundo",
		Role:      database.ADMIN,
		Games:     nil,
		Messages:  nil,
	}}

var GamesList = []database.Game{
	{
		Name:       "Game 1",
		MaxPlayers: 4,
		Owners:     nil,
	},

	{
		Name:       "Game 2",
		MaxPlayers: 6,
		Owners:     nil,
	},
	{
		Name:       "Game 3",
		MaxPlayers: 2,
		Owners:     nil,
	},
	{
		Name:       "Game 4",
		MaxPlayers: 2,
		Owners:     nil,
	},
}

var NonAddedGame = database.Game{
	Name:       "Game the first",
	MaxPlayers: 2,
	Owners:     nil,
}

func SetupDB(t *testing.T) {
	if _, err := os.Stat(DBURI); err == nil {
		os.Remove(DBURI)
	}

	err := database.ConnectToDB(DBTYPE, DBURI)
	if err != nil {
		t.Error()
	}

	err = database.InsertUser(UsersList[0])
	if err != nil {
		t.Error(err)
	}

	err = database.InsertUser(UsersList[1])
	if err != nil {
		t.Error(err)
	}

	err = database.InsertUser(UsersList[2])
	if err != nil {
		t.Error(err)
	}

	err = database.InsertGame(GamesList[0])
	if err != nil {
		t.Error(err)
	}

	err = database.InsertGame(GamesList[1])
	if err != nil {
		t.Error(err)
	}

	err = database.InsertGame(GamesList[2])
	if err != nil {
		t.Error(err)
	}

	err = database.InsertGame(GamesList[3])
	if err != nil {
		t.Error(err)
	}

	err = database.InsertGameUserAssoc(GamesList[0], UsersList[0])
	if err != nil {
		t.Error(err)
	}

	err = database.InsertGameUserAssoc(GamesList[1], UsersList[0])
	if err != nil {
		t.Error(err)
	}

	err = database.InsertGameUserAssoc(GamesList[2], UsersList[0])
	if err != nil {
		t.Error(err)
	}

	err = database.InsertGameUserAssoc(GamesList[3], UsersList[1])
	if err != nil {
		t.Error(err)
	}

	err = database.InsertGameUserAssoc(GamesList[1], UsersList[1])
	if err != nil {
		t.Error(err)
	}
}

func TeardownDB(t *testing.T) {
	err := database.CloseDB()
	if err != nil {
		t.Error()
	}
}

func TestAddGame(t *testing.T) {
	SetupDB(t)
	defer TeardownDB(t)

	response, err := AddGame(UsersList[0].DiscordID, NonAddedGame.Name, NonAddedGame.MaxPlayers)
	if err != nil {
		t.Error(err)
	}

	for _, r := range response {
		if r.ShouldRespondToChannel != false ||
			r.ShouldRespondToUser != false ||
			strings.Compare(r.Message, "") != 0 ||
			len(r.ReactionsToAdd) != 0 {
			t.Error(errAssertFail)
		}
	}
}

func TestDeop(t *testing.T) {
	SetupDB(t)
	defer TeardownDB(t)

	response, err := Deop(UsersList[2].DiscordID)
	if err != nil {
		t.Error(err)
	}
	for _, r := range response {
		if r.ShouldRespondToChannel != false ||
			r.ShouldRespondToUser != false ||
			strings.Compare(r.Message, "") != 0 ||
			len(r.ReactionsToAdd) != 0 {
			t.Error(errAssertFail)
		}
	}
}

func TestDisable(t *testing.T) {
	SetupDB(t)
	defer TeardownDB(t)

	response, err := Disable(UsersList[2].DiscordID)
	if err != nil {
		t.Error(err)
	}

	for _, r := range response {
		if r.ShouldRespondToChannel != false ||
			r.ShouldRespondToUser != false ||
			strings.Compare(r.Message, "") != 0 ||
			len(r.ReactionsToAdd) != 0 {
			t.Error(errAssertFail)
		}
	}
}

func TestEnable(t *testing.T) {
	SetupDB(t)
	defer TeardownDB(t)

	response, err := Enable(UsersList[1].DiscordID)
	if err != nil {
		t.Error(err)
	}

	for _, r := range response {
		if r.ShouldRespondToChannel != false ||
			r.ShouldRespondToUser != false ||
			strings.Compare(r.Message, "") != 0 ||
			len(r.ReactionsToAdd) != 0 {
			t.Error(errAssertFail)
		}
	}
}

func TestGames(t *testing.T) {
	SetupDB(t)
	defer TeardownDB(t)

	response, err := Games(3)
	if err != nil {
		t.Error(err)
	}

	for _, r := range response {
		if r.ShouldRespondToChannel != true ||
			r.ShouldRespondToUser != false ||
			len(r.Message) != 44 ||
			len(r.ReactionsToAdd) != 0 {
			t.Error(errAssertFail)
		}
	}
}

func TestInspire(t *testing.T) {
	SetupDB(t)
	defer TeardownDB(t)

	response, err := Inspire()
	if err != nil {
		t.Error(err)
	}

	for _, r := range response {

		defer r.VoiceOutput.Cleanup()

		if r.ShouldRespondToChannel != true ||
			r.ShouldRespondToUser != false ||
			len(r.Message) == 0 ||
			len(r.ReactionsToAdd) != 0 {
			t.Error(errAssertFail)
		}
	}
}

func TestLaugh(t *testing.T) {
	SetupDB(t)
	defer TeardownDB(t)

	response, err := Laugh()
	if err != nil {
		t.Error(err)
	}

	for _, r := range response {

		defer r.VoiceOutput.Cleanup()

		if r.ShouldRespondToChannel != false ||
			r.ShouldRespondToUser != false ||
			len(r.Message) != 0 ||
			len(r.ReactionsToAdd) != 0 {
			t.Error(errAssertFail)
		}
	}
}

func TestHelp(t *testing.T) {
	SetupDB(t)
	defer TeardownDB(t)

	response, err := Help(UsersList[1].DiscordID)
	if err != nil {
		t.Error(err)
	}

	for _, r := range response {

		if r.ShouldRespondToChannel != false ||
			r.ShouldRespondToUser != true ||
			len(r.Message) == 0 ||
			len(r.ReactionsToAdd) != 0 {
			t.Error(errAssertFail)
		}
	}
}

func TestOp(t *testing.T) {
	SetupDB(t)
	defer TeardownDB(t)

	response, err := Op(UsersList[1].DiscordID)
	if err != nil {
		t.Error(err)
	}

	for _, r := range response {

		if r.ShouldRespondToChannel != false ||
			r.ShouldRespondToUser != false ||
			strings.Compare(r.Message, "") != 0 ||
			len(r.ReactionsToAdd) != 0 {
			t.Error(errAssertFail)
		}
	}
}

func TestPlay(t *testing.T) {
	SetupDB(t)
	defer TeardownDB(t)

	response, err := Play(HappyJinjoHouse)
	if err != nil {
		t.Error(err)
	}

	for _, r := range response {

		defer r.VoiceOutput.Cleanup()

		if r.ShouldRespondToChannel != false ||
			r.ShouldRespondToUser != false ||
			strings.Compare(r.Message, "") != 0 ||
			len(r.ReactionsToAdd) != 0 {
			t.Error(errAssertFail)
		}
	}
}

func TestPoll(t *testing.T) {
	SetupDB(t)
	defer TeardownDB(t)

	response, err := Poll([]string{
		"Banjo",
		"Geno",
		"A Fucking Pirannah Plant",
	})
	if err != nil {
		t.Error(err)
	}

	for _, r := range response {

		if r.ShouldRespondToChannel != true ||
			r.ShouldRespondToUser != false ||
			len(r.Message) == 0 ||
			len(r.ReactionsToAdd) != 3 {
			t.Error(errAssertFail)
		}
	}
}

func TestReg(t *testing.T) {
	SetupDB(t)
	defer TeardownDB(t)

	response, err := Reg(UsersList[1].DiscordID, "Waluigi")
	if err != nil {
		t.Error(err)
	}

	for _, r := range response {

		if r.ShouldRespondToChannel != false ||
			r.ShouldRespondToUser != false ||
			strings.Compare(r.Message, "") != 0 ||
			len(r.ReactionsToAdd) != 0 {
			t.Error(errAssertFail)
		}
	}
}

func TestSource(t *testing.T) {
	SetupDB(t)
	defer TeardownDB(t)

	response, err := Source()
	if err != nil {
		t.Error(err)
	}

	for _, r := range response {

		if r.ShouldRespondToChannel != true ||
			r.ShouldRespondToUser != false ||
			strings.Compare(r.Message, "I am currently hosted @ https://gitlab.com/the-tito-foundation/tito") != 0 ||
			len(r.ReactionsToAdd) != 0 {
			t.Error(errAssertFail)
		}
	}
}

func TestWhois(t *testing.T) {
	SetupDB(t)
	defer TeardownDB(t)

	response, err := WhoIs(UsersList[2].DiscordID)
	if err != nil {
		t.Error(err)
	}

	for _, r := range response {

		if r.ShouldRespondToChannel != false ||
			r.ShouldRespondToUser != true ||
			strings.Compare(r.Message, fmt.Sprintf("User %s is %s", UsersList[2].Nickname, UsersList[2].RealName)) != 0 ||
			len(r.ReactionsToAdd) != 0 {
			t.Error(errAssertFail)
		}
	}
}

func TestSummon(t *testing.T) {
	SetupDB(t)
	defer TeardownDB(t)

	mentionTag := "<!@DEADBEEF>"

	response, err := Summon(mentionTag)
	if err != nil {
		t.Error(err)
	}

	for _, r := range response {

		if r.ShouldRespondToChannel != true ||
			r.ShouldRespondToUser != false ||
			strings.Compare(r.Message, fmt.Sprintf(mentionTag)) != 0 ||
			len(r.ReactionsToAdd) != 0 {
			t.Error(errAssertFail)
		}
	}
}
