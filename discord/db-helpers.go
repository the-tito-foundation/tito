package discord

import (
	"log"
	"time"

	"github.com/bwmarrin/discordgo"
	"gitlab.com/the-tito-foundation/tito/config"
	"gitlab.com/the-tito-foundation/tito/database"
)

func addElementsToDB(m *discordgo.MessageCreate) (database.User, error) {

	b, err := database.IsInMsgDB(database.Message{MessageContent: m.Content})
	if err != nil {
		return database.User{}, err
	}

	if !b {
		err := database.InsertMessage(database.Message{DateFirstSent: time.Now(), MessageContent: m.Content})
		if err != nil {
			log.Println(err)
			return database.User{}, err
		}
	}

	member, err := session.GuildMember(config.Cfg.GuildConfig.GuildID, m.Author.ID)
	if err != nil {
		log.Println(err)
		return database.User{}, err
	}

	b, err = database.IsInUserDB(database.User{DiscordID: m.Author.ID})
	if err != nil {
		return database.User{}, err
	}

	if !b {
		err = database.InsertUser(database.User{DiscordID: m.Author.ID, Username: m.Author.Username, Nickname: member.Nick})
		if err != nil {
			return database.User{}, err
		}
	} else {
		err = database.UpdateUser(database.User{DiscordID: m.Author.ID, Username: m.Author.Username, Nickname: member.Nick})
		if err != nil {
			return database.User{}, err
		}
	}

	u, err := database.SelectUser(database.User{DiscordID: m.Author.ID})
	if err != nil {
		return database.User{}, err
	}

	msg, err := database.SelectMessage(database.Message{MessageContent: m.Content})
	if err != nil {
		return u, err
	}

	err = database.InsertUserMessageAssoc(msg, u)
	if err != nil {
		return u, err
	}

	return u, nil
}
