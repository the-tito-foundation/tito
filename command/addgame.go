package command

import (
	"gitlab.com/the-tito-foundation/tito/database"
)

// AddGame - Register a game to the user that requested the !addgame command.
func AddGame(discordID, gameName string, maxPlayers uint8) ([]Response, error) {
	u := database.User{DiscordID: discordID}
	g := database.Game{Name: gameName, MaxPlayers: maxPlayers}

	b, err := database.IsInGameDB(g)
	if err != nil {
		return []Response{}, err
	}

	if !b {
		err = database.InsertGame(g)
		if err != nil {
			return []Response{}, err
		}
	}

	g, err = database.SelectGame(g)
	if err != nil {
		return []Response{}, err
	}

	u, err = database.SelectUser(u)
	if err != nil {
		return []Response{}, err
	}

	err = database.InsertGameUserAssoc(g, u)
	if err != nil {
		return []Response{}, err
	}

	response := Response{
			ShouldRespondToChannel: false,
			ShouldRespondToUser:    false,
			Message:                "",
		}

	return []Response{response}, nil

}
