package database

import (
	"errors"
	"log"
)

var errDoesNotExist = errors.New("record does not exist")

// UpdateUser - Update a User (if it exists)
func UpdateUser(user User) error {
	var err error

	user.Games = nil
	user.Messages = nil

	ok, err := IsInUserDB(User{DiscordID: user.DiscordID})
	if err != nil || !ok {
		if err == nil {
			log.Println(errDoesNotExist)
			return errDoesNotExist
		}
		log.Println(err)
		return err
	}

	err = dbConn.Model(&User{}).Where(&User{DiscordID: user.DiscordID}).Updates(&user).Error
	if err != nil {
		log.Println(err)
	}
	return err
}

// UpdateGame - Update a Game (if it exists)
func UpdateGame(game Game) error {
	var err error

	game.Owners = nil

	ok, err := IsInGameDB(Game{Name: game.Name})
	if err != nil || !ok {
		if err == nil {
			log.Println(errDoesNotExist)
			return errDoesNotExist
		}
		log.Println(err)
		return err
	}

	err = dbConn.Model(&Game{}).Where(&Game{Name: game.Name}).Updates(&game).Error
	if err != nil {
		log.Println(err)
	}
	return err
}

// UpdateMessage - Update a Message (if it exists?)
func UpdateMessage(message Message) error {
	var err error

	message.Senders = nil

	ok, err := IsInMsgDB(Message{MessageContent: message.MessageContent})
	if err != nil || !ok {
		if err == nil {
			log.Println(errDoesNotExist)
			return errDoesNotExist
		}
		log.Println(err)
		return err
	}

	err = dbConn.Model(&Message{}).Where(&Message{MessageContent: message.MessageContent}).Updates(&message).Error
	if err != nil {
		log.Println(err)
	}
	return err
}
