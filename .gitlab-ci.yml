image: registry.gitlab.com/the-tito-foundation/tito/tito-archlinux-gobuild:latest

stages:
  - build
  - test
  - analysis
  - release
  - bundle

.go_build_before_script:
  before_script:
    - go get -v -d ./...
    - chmod 777 ./build/coverage.sh
    - export PKG_LIST=$(go list gitlab.com/the-tito-foundation/tito/... | grep -v /vendor/ | grep -v /resources)
    - export PKG=gitlab.com/the-tito-foundation/tito
    - cd $GOPATH/src/$PKG
    - git checkout $CI_COMMIT_SHA

build:
  extends: .go_build_before_script
  stage: build
  script:
    - go build -i -v $PKG
  
build-embed:
  extends: .go_build_before_script
  stage: build
  allow_failure: true
  script:
    - go build -i -v -tags embed $PKG

unit_tests:
  extends: .go_build_before_script
  stage: test
  script:
    - go test -short $PKG_LIST

race_detector:
  extends: .go_build_before_script
  stage: test
  script:
    - go test -race -short $PKG_LIST

memory_sanitizer:
  extends: .go_build_before_script
  stage: test
  script:
    - go test -msan -short $PKG_LIST

code_coverage:
  extends: .go_build_before_script
  stage: analysis
  script:
    - make coverage

lint:
  extends: .go_build_before_script
  stage: analysis
  script:
    - golint -set_exit_status $PKG_LIST

x64-unix:
  extends: .go_build_before_script
  only:
    - master@the-tito-foundation/tito
  variables:
    GOOS: linux
    GOARCH: amd64
  stage: release
  script:
    - go build -i -v -o $GOPATH/bin/tito $PKG
    - mkdir artifacts
    - cp $GOPATH/bin/tito artifacts
    - find resources -name "*.ogg" | cpio -pdm artifacts 
    - tar -cvf artifacts/tito-$CI_JOB_NAME.tar.gz artifacts/tito-artifacts/resources
  artifacts:
    paths:
      - artifacts/tito-$CI_JOB_NAME.tar.gz

x86-unix:
  extends: .go_build_before_script
  only:
    - master@the-tito-foundation/tito
  allow_failure: true
  variables:
    GOOS: linux
    GOARCH: "386"
  stage: release
  script:
    - go build -i -v -o $GOPATH/bin/tito $PKG
    - mkdir artifacts
    - cp $GOPATH/bin/tito artifacts
    - find resources -name "*.ogg" | cpio -pdm artifacts 
    - tar -cvf artifacts/tito-$CI_JOB_NAME.tar.gz artifacts/tito-artifacts/resources
  artifacts:
    paths:
      - artifacts/tito-$CI_JOB_NAME.tar.gz

arm64-unix:
  extends: .go_build_before_script
  only:
    - master@the-tito-foundation/tito
  allow_failure: true
  variables:
    GOOS: linux
    GOARCH: arm64
  stage: release
  script:
    - go build -i -v -o $GOPATH/bin/tito $PKG
    - mkdir artifacts
    - cp $GOPATH/bin/tito artifacts
    - find resources -name "*.ogg" | cpio -pdm artifacts 
    - tar -cvf artifacts/tito-$CI_JOB_NAME.tar.gz artifacts/tito-artifacts/resources
  artifacts:
    paths:
      - artifacts/tito-$CI_JOB_NAME.tar.gz

arm-unix:
  extends: .go_build_before_script
  only:
    - master@the-tito-foundation/tito
  allow_failure: true
  variables:
    GOOS: linux
    GOARCH: arm
  stage: release
  script:
    - go build -i -v -o $GOPATH/bin/tito $PKG
    - mkdir artifacts
    - cp $GOPATH/bin/tito artifacts
    - find resources -name "*.ogg" | cpio -pdm artifacts 
    - tar -cvf artifacts/tito-$CI_JOB_NAME.tar.gz artifacts/tito-artifacts/resources
  artifacts:
    paths:
      - artifacts/tito-$CI_JOB_NAME.tar.gz

x64-win32:
  extends: .go_build_before_script
  only:
    - master@the-tito-foundation/tito
  allow_failure: true
  variables:
    GOOS: windows
    GOARCH: amd64
  stage: release
  script:
    - go build -i -v -o $GOPATH/bin/tito $PKG
    - mkdir artifacts
    - cp $GOPATH/bin/tito artifacts
    - find resources -name "*.ogg" | cpio -pdm artifacts 
    - tar -cvf artifacts/tito-$CI_JOB_NAME.tar.gz artifacts/tito-artifacts/resources
  artifacts:
    paths:
      - artifacts/tito-$CI_JOB_NAME.tar.gz

x86-win32:
  extends: .go_build_before_script
  only:
    - master@the-tito-foundation/tito
  allow_failure: true
  variables:
    GOOS: windows
    GOARCH: "386"
  stage: release
  script:
    - go build -i -v -o $GOPATH/bin/tito $PKG
    - mkdir artifacts
    - cp $GOPATH/bin/tito artifacts
    - find resources -name "*.ogg" | cpio -pdm artifacts 
    - tar -cvf artifacts/tito-$CI_JOB_NAME.tar.gz artifacts/tito-artifacts/resources
  artifacts:
    paths:
      - artifacts/tito-$CI_JOB_NAME.tar.gz

arm64-win32:
  extends: .go_build_before_script
  only:
    - master@the-tito-foundation/tito
  allow_failure: true
  variables:
    GOOS: windows
    GOARCH: arm64
  stage: release
  script:
    - go build -i -v -o $GOPATH/bin/tito $PKG
    - mkdir artifacts
    - cp $GOPATH/bin/tito artifacts
    - find resources -name "*.ogg" | cpio -pdm artifacts 
    - tar -cvf artifacts/tito-$CI_JOB_NAME.tar.gz artifacts/tito-artifacts/resources
  artifacts:
    paths:
      - artifacts/tito-$CI_JOB_NAME.tar.gz

arm-win32:
  extends: .go_build_before_script
  only:
    - master@the-tito-foundation/tito
  allow_failure: true
  variables:
    GOOS: windows
    GOARCH: arm
  stage: release
  script:
    - go build -i -v -o $GOPATH/bin/tito $PKG
    - mkdir artifacts
    - cp $GOPATH/bin/tito artifacts
    - find resources -name "*.ogg" | cpio -pdm artifacts 
    - tar -cvf artifacts/tito-$CI_JOB_NAME.tar.gz artifacts/tito-artifacts/resources
  artifacts:
    paths:
      - artifacts/tito-$CI_JOB_NAME.tar.gz

# build-container:
#   image: docker
#   only:
#     - master@the-tito-foundation/tito
#   stage: bundle
#   script:
#     - curl -gL 'https://gitlab.com/the-tito-foundation/tito/-/jobs/artifacts/master/download?job=x64-unix' -o ./tito-x64-nix.tar.gz
#     - tar -xvf ./tito-x64-nix.tar.gz
#     - tar -xvf ./artifacts/tito-x64-unix.tar.gz
#     - mv ./artifacts/tito .
#     - mv ./artifacts/resources .
#     - docker login $CI_REGISTRY -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD
#     - docker pull $CI_REGISTRY/the-tito-foundation/tito/tito-archlinux-go
#     - docker build -f Dockerfile.run -t $CI_REGISTRY/the-tito-foundation/tito/tito-runtime .
#     - docker push $CI_REGISTRY/the-tito-foundation/tito/tito-runtime
