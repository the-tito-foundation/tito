package command

import (
	"fmt"
	"log"

	"gitlab.com/the-tito-foundation/tito/resources"
)

const embedHumanFstr = "embed://vote/human/%d"
const embedReactionFStr = "embed://vote/computer/%d"

// Poll - start a poll for the indicated options.
func Poll(options []string) ([]Response, error) {
	if len(options) == 0 {
		log.Println(errNoOptions)
		return []Response{}, errNoOptions
	}

	str := ""

	for k, v := range options {
		resource, err := resources.LoadResource(fmt.Sprintf(embedHumanFstr, k))
		if err != nil {
			return []Response{}, err
		}

		str += fmt.Sprintf("%s %s\n", resource.Raw.(string), v)
	}

	reactions := make([]string, 0)
	for k := range options {
		resource, err := resources.LoadResource(fmt.Sprintf(embedReactionFStr, k))
		if err != nil {
			return []Response{}, err
		}

		reactions = append(reactions, resource.Raw.(string))
	}

	response := Response{
		ShouldRespondToChannel: true,
		ShouldRespondToUser:    false,
		Message:                str,
		ReactionsToAdd:         reactions,
	}

	return []Response{response}, nil

}
